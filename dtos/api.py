from email import message
from pydantic import BaseModel
from datetime import datetime
from enum import Enum

class StatusCodes(Enum):
     OK = 200
     CREATED = 201
     NOT_FOUND = 401

class Response(BaseModel):
        message: str
        status_code: StatusCodes
        date: datetime = datetime.now()
