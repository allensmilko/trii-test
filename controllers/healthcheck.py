from dtos.api import StatusCodes

def healthcheck():
    return {
            "message": "OK",
            "status_code": StatusCodes.OK
            }