from fastapi import FastAPI
from controllers import healthcheck
from dtos.api import Response

app = FastAPI()


@app.get('/healthcheck', response_model=Response)
async def healthcheck_rest():
    result = healthcheck.healthcheck()
    return result